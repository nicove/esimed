package fr.esimed.tp


import org.junit.jupiter.api.Test
import  org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.function.Executable

//import java.lang.reflect.Executable


class TextToolsTests {


 val distance = fr.esimed.tp.distance ()

@Test
 fun levenshteinDistanceTest(){
 Assertions.assertAll(
  Executable { Assertions.assertEquals(6, distance.levenshteinDistance("Arnaud", "Julien")) },
  Executable { Assertions.assertEquals(1, distance.levenshteinDistance("Julie", "Julien")) }
 )


}

}
